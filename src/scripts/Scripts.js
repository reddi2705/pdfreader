import $ from 'jquery'

export default class Scripts {
    constructor() {
        $(document).ready(function () {
            $(window).keydown(function (event) {
                switch (event.keyCode) {
                    case 37:
                        if ($('.Page').data('direction') === 'rtl') {
                            $('button.owlcarousel-next').click();
                        } else {
                            $('button.owlcarousel-prev').click();
                        }
                        break;
                    case 39:
                        if ($('.Page').data('direction') === 'rtl') {
                            $('button.owlcarousel-prev').click();
                        } else {
                            $('button.owlcarousel-next').click();
                        }
                        break;
                    default:
                        break;
                }
            });

            let activityTimeout = setTimeout(inActive, 5000);

            function resetActive() {
                if ($('.DisabledArea').length === 0) {
                    $('.Buttons').removeAttr('style');
                    $('.MetaData').removeAttr('style');
                    $('.PrevPage').removeAttr('style');
                    $('.NextPage').removeAttr('style');
                    $('.Disclamer').removeAttr('style');
                    clearTimeout(activityTimeout);
                    activityTimeout = setTimeout(inActive, 5000);
                }
            }

            function inActive() {
                const direction = $('body').data('direction');
                if ($('.DisabledArea').length === 0) {
                    $('.Buttons').css({bottom: (-1 * $('.Buttons').height())}, 500, 'linear');
                    $('.Disclamer').css({top: (-1 * $('.Disclamer').height())}, 500, 'linear');
                    if (direction === 'ltr') {
                        $('.PrevPage').css({left: (-1 * ($('.PrevPage').width() + 20))}, 500, 'linear');
                        $('.NextPage').css({right: (-1 * ($('.NextPage').width() + 20))}, 500, 'linear');
                    } else {
                        $('.PrevPage').css({right: (-1 * ($('.PrevPage').width() + 20))}, 500, 'linear');
                        $('.NextPage').css({left: (-1 * ($('.NextPage').width() + 20))}, 500, 'linear');
                    }
                    if(Scripts.isMobile()) {
                        $('.MetaData').css({top: -50}, 500, 'linear');
                    } else {
                        $('.MetaData').css({bottom: -50}, 500, 'linear');
                    }
                }
            }

            $(document).bind(
                'mouseenter mousedown mouseup mousedrag mouseleave mousemove click touchstart touchend', function () {
                resetActive();
            });

            window.addEventListener('appinstalled', (evt) => {
                alert('a2hs installed');
            });
        });
    }

    static isPortrait = () => {
        const width = $('body').width();
        const height = $('body').height();

        return (width < height);
    };

    static isMobile = () => {
        return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
    };

    static parseLink = (html) => {
        const pattern = /((w(<[^>]+>)*){3}\.([a-z0-9_\-.](<[^>]+>)*)*?\.(((<[^>]+>)*c(<[^>]+>)*z)|((<[^>]+>)*s(<[^>]+>)*k)))/gmi;
        const found = html.match(pattern);
        let modifiedHtml = '';
        if (found !== null) {
            const foundLength = found.length;

            for (let i = 0; i < foundLength; ++i) {
                modifiedHtml = html.replace(pattern, (link) => {
                    return '<a class="link" target="_blank" rel="noopener noreferrer" href="http://' + link.replace(/(<([^>]+)>)/ig, "") + '">' + link.replace(/(<([^>]+)>)/ig, "") + '</a>'
                });
            }
        } else {
            modifiedHtml = html;
        }

        return modifiedHtml;
    };

    static clone = (src) => {
        return Object.assign({}, src);
    };

    static range = (start, end, step = 1) => {
        const allNumbers = [start, end, step].every(Number.isFinite);
        if (!allNumbers) {
            throw new TypeError('range() expects only finite numbers as arguments.');
        }
        if (step <= 0) {
            throw new Error('step must be a number greater than 0.');
        }
        if (start > end) {
            step = -step;
        }
        const length = Math.floor(Math.abs((end - start) / step)) + 1;
        return Array.from(Array(length), (x, index) => start + index * step);
    };

    static is = (x, y) => {
        if (x === y) {
            return x !== 0 || y !== 0 || 1 / x === 1 / y;
        } else {
            return false;
        }
    };

    static compareObjects = (objA, objB, excludeKeys) => {
        if (Scripts.is(objA, objB)) {
            return true;
        }

        if (typeof objA !== 'object' || objA === null || typeof objB !== 'object' || objB === null) {
            return false;
        }

        var keysA = Object.keys(objA);
        var keysB = Object.keys(objB);

        if (keysA.length !== keysB.length) {
            return false;
        }

        for (var i = 0; i < keysA.length; i++) {
            const k = keysA[i];
            if (excludeKeys.indexOf(k) !== -1) {
                continue;
            }
            if (!hasOwnProperty.call(objB, k) || !Scripts.is(objA[k], objB[k])) {
                return false;
            }
        }

        return true;
    }
}
