const { join, resolve } = require('path');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WorkboxWebpackPlugin = require('workbox-webpack-plugin');

module.exports = {
  entry: join(__dirname, 'src', 'index.js'),
  output: {
    filename: 'js/app.bundle.js',
    path: resolve(__dirname, 'dist'),
    publicPath: '/'
  },
  module: {
    rules: [
      {
        include: resolve(__dirname, "./src/"),
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              "@babel/preset-env",
              "@babel/preset-react"
            ],
            plugins: [
              "@babel/plugin-syntax-dynamic-import",
              "@babel/plugin-proposal-class-properties"
            ]
          }
        }
      },
      {
        test: /\.html$/,
        use: [{
          loader: "html-loader"
        }]
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({ 
          fallback: 'style-loader',
          use: ['css-loader'],
        })
      },
    ],
  },
  devServer: {
    contentBase: resolve(__dirname, 'app'),
    historyApiFallback: true
  },
  resolve: {
    modules: [
      'node_modules', 'src'
    ],
    extensions: [
      '*', '.js', '.jsx', '.css'
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'css/app.bundle.css'
    }),
    new HtmlWebPackPlugin({
      template: join(__dirname, 'public', 'index.html')
    }),
    new CopyWebpackPlugin([
      { from: 'public' }
    ]),
    new WorkboxWebpackPlugin.GenerateSW({
      exclude: [/\.map$/, /asset-manifest\.json$/, /index.html/],
    })
    /* new WorkboxWebpackPlugin.InjectManifest({
      swSrc: './src/serviceWorker.js',
      swDest: 'service-worker.js'
    }) */
  ]
};
