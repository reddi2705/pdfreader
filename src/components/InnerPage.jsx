/* eslint-disable */

import React, {Component} from "react";
import axios from "axios"
import Scripts from "../scripts/Scripts"

export default class InnerPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            html: ""
        }
    }

    componentDidMount() {
        /* this.getPage(this.props.extension); */
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (
            (this.props.isLandscape !== nextProps.isLandscape)
            || (!this.props.isLandscape && (this.props.pageActual !== nextProps.pageActual))
            || nextProps.extension !== this.props.extension
            || !Scripts.compareObjects(nextState.html, this.state.html, [])
        );
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (
            prevProps.extension !== this.props.extension
            || !Scripts.compareObjects(prevState.html, this.state.html, [])
        ) {
            this.forceUpdate();
            this.props.adjustPage();
        }
    }

    render() {
        const regex = /(<img.+src=")(.+)?("\/?>)/m;
        const matches = this.props.pageHtml.match(regex);
        let html = '';

        /* this.getPage(this.props.extension); */

        if (matches !== null) {
            if (matches[2] !== undefined) {
                const realImage = `${matches[2].split('.')[0]}.${this.props.extension}`;
                html = this.props.pageHtml.replace(
                    regex,
                    `
                        ${matches[1]}${this.url(this.props.extension)}${realImage}" onLoad="${this.onImageLoaded()}" draggable="false"${matches[3]}
                    `
                );
            } else {
                html = this.props.pageHtml.replace(
                    regex,
                    `
                        ${matches[1]}${this.url(this.props.extension)}bg${this.props.pageNumber.toString(16)}.${this.props.extension}" onLoad="${this.onImageLoaded()}"draggable="false"${matches[3]}
                    `
                );
            }
        } else {
            const matchImg = this.props.pageHtml.match(/(<div[^>]+class\s*=\s*"pc.*?)(<\/div>)/m);

            html = this.props.pageHtml.replace(
                /(<div[^>]+class\s*=\s*"pc.*?)(<\/div>)/m, 
                `${matchImg[1]}<img class="w100 h100" src="${this.url()}bg${this.props.pageNumber.toString(16)}.${this.props.extension}" onLoad="${this.onImageLoaded()}" draggable="false" />${matchImg[2]}`
            );
        }

        const isLandscape = this.props.isLandscape;
        const showMyself =  isLandscape || (this.props.pageNumber === parseInt(this.props.pageActual));

        return (
            <div
                dangerouslySetInnerHTML={{__html: html}}
                className={`PageItem ${this.props.extension}`}
                style={{display: showMyself ? 'inline-block' : 'none'}}
                ref={this.props.refInnerPage}
            />
        )
    }

    onImageLoaded = () => {
        this.props.bindImageLoader();
    };

    /* getPage(quality) {
        const url = this.buildUrl(quality, this.props.pageNumber);
        axios
            .get(url)
            .then(response => {
                this.setState({ html: response.data });
            })
            .catch(error => {
                console.log(error);
            });
    }; */

    buildUrl = (quality, page) => {
        return `${this.url(quality)}${this.props.name}${page}.page`;
    }

    url = (quality) => {
        return `${window.location.protocol}//${window.location.hostname}:${window.location.port}/data/${this.props.provider}/${this.props.name}/${quality}/`;
    };
}
