/* eslint-disable */

import React, {Component} from 'react'
import Language from "./Language"

class QualitySwitcher extends Component {
    constructor(props) {
        super(props);

        this.state = {
            opened: false,
            value: this.props.quality,
        }

        this.open = this.open.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.props.open === nextProps.open;
    }

    render() {
        const {
            text,
            translation,
            allowedBackgroundFormats
        } = this.props;
        return (
            <React.Fragment>
                <div className={`QualitySwitcher`}>
                    <button className={`icon-settings`} onClick={(e) => this.open(e)}></button>
                </div>
                {
                    this.state.open ?
                    <div className={`Settings ${this.props.language}`}>
                        <React.Fragment>
                            <h3 className={`Settings__title`}><Language text={text} translation={translation} /></h3>
                            {
                                allowedBackgroundFormats.map((format) => {
                                    const trans = <Language text={`format_${format}`} translation={translation} />;
                                    const checked = (format === this.state.value);
                                    return (
                                        <div className={`radiocheck`} key={format}>
                                            <input
                                                type="radio"
                                                id={`Quality${format}`}
                                                checked={checked}
                                                value={format}
                                                className={`radiocheck__input`}
                                                name={`quality`}
                                                onChange={this.handleChange} />
                                            <label className={`radiocheck__label`} htmlFor={`Quality${format}`}>{trans.props.translation[trans.props.text]}<span className={`label__sup`}>{format}</span></label>
                                        </div>
                                    )
                                })
                            }
                        </React.Fragment>
                    </div> : ''
                }
            </React.Fragment>
        );
    }

    open = () => {
        this.setState({'open': !this.state.open});
    }

    handleChange = e => {
        this.setState({value: e.target.value});
        this.props.callbackFromAction(e.target.value);
        this.setState({'open': false});
    }
}

export default QualitySwitcher
