/* eslint-disable */

import React, {Component} from 'react';

export default class MetaData extends Component {
    constructor(props) {
//        console.clear();

        super(props);
    }

    render() {
        const {
            pageCount,
            connectedPage,
            actualPage,
            pageShowed
        } = this.props;

        const pageNum = parseInt(actualPage);
        let pagination = null;
        if (connectedPage === 0 && pageShowed === 1) {
            pagination = pageNum;
        } else if (connectedPage === 0 && pageShowed === 2 && pageShowed === pageCount) {
            pagination = actualPage + '-' + pageCount;
        } else if (connectedPage >= pageCount) {
            pagination = pageNum;
        } else {
            pagination = connectedPage < pageNum ? connectedPage + '-' + pageNum : pageNum + '-' + connectedPage;
        }

        return (
            <div className={`MetaData`}>
                <div className={`PageCount`}>{pagination} / {pageCount}</div>
                <div className={`PageCountProgress`}><div style={{width: (pageNum / pageCount) * 100 + '%'}} /></div>
            </div>
        );
    }
}
