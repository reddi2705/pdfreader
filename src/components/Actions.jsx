/* eslint-disable */

import React, {Component} from 'react';
import axios from "axios";
import Thumbnails from "./Thumbnails";
import MetaData from "./MetaData";
import Language from "./Language"
import QualitySwitcher from "./QualitySwitcher"


class Actions extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loadingLower: false,
            loadingHigher: false,
            translation: {},
            error: '',
            qualitySwitcher: props.backgroundFormat,
            open: false,
            info: false
        };

        this.read = this.read.bind(this);
        this.fullscreenExpand = this.fullscreenExpand.bind(this);
        this.language = this.resolveLanguage(props);
    }

    componentDidMount() {
        axios
            .get(`/translations/${this.language}.json`)
            .then(res => this.setState({ translation: res.data }))
            .catch(error => this.setState({ error }));
    }

    render() {
        let datePublish = new Date(this.props.datePublish);
        const actionsClass = this.state.info ? 'Actions ActionsOpened' : 'Actions';

        return (
            <React.Fragment>
                {this.props.skin === "crew" ? <div className={`Blood`}></div> : ""}
                {this.state.info && <div className={`DisabledArea`}/>}
                {
                    this.state.info ? (
                        <div className={`Info`} onClick={(e) => this.read(e)}>
                            <div className={`InfoContainer`}>
                                <div className={`InfoContainerInner`}>
                                    <h1 className={`InfoTitle`}>{this.props.titleName ? this.props.titleName : this.props.name}</h1>
                                    <dl className={`row InfoDescription`}>
                                        {
                                            this.props.datePublish ?
                                                <React.Fragment>
                                                    <dt className={`col-sm-3`}><Language translation={this.state.translation} text='date_published' />:</dt>
                                                    <dd className={`col-sm-9`}>{`${datePublish.getDate()}. ${datePublish.getMonth() + 1}. ${datePublish.getFullYear()}`}</dd><br/>
                                                </React.Fragment> : ''
                                        }
                                        <dt className={`col-sm-3`}><Language translation={this.state.translation} text='page_count' />:</dt>
                                        <dd className={`col-sm-9`}>{this.props.pageCount}</dd><br/>
                                        <dt className={`col-sm-3`}><Language translation={this.state.translation} text='publisher' />:</dt>
                                        <dd className={`col-sm-9`}>{this.props.publisher ? this.props.publisher : this.props.provider}</dd>
                                    </dl>
                                    <div className={`InfoBar ${this.language}`}><span><Language translation={this.state.translation} text='info' /></span></div>
                                </div>
                            </div>
                        </div>
                    )
                    : ''
                }

                <div className={`${actionsClass}`}>
                    <div className={`Buttons`}>
                        <div className={`Read`}>
                            <button type={`button`} className={`icon-menu ${this.language}`} onClick={(e) => this.read(e)}>
                                <span className={`BtnClosedText`}><Language translation={this.state.translation} text='more' /></span>
                                <span className={`BtnOpenedText`}><Language translation={this.state.translation} text='read' /></span>
                            </button>
                        </div>
                        {
                            this.isInIframe() ?
                                <div className={`Fullscreen`}>
                                    <button type={`button`} className={`icon-size-fullscreen`} onClick={(e) => this.fullscreenExpand(e)}/>
                                </div> : ""
                        }
                        {
                            this.props.allowedBackgroundFormats.includes(this.props.backgroundFormat) ?
                                <QualitySwitcher open={this.state.open}
                                                 text='choose_quality'
                                                 language={this.language}
                                                 translation={this.state.translation}
                                                 callbackFromAction={this.callbackFromAction}
                                                 backgroundFormat={this.props.backgroundFormat}
                                                 allowedBackgroundFormats={this.props.allowedBackgroundFormats}
                                                 quality={this.props.backgroundFormat}
                                /> : ''
                        }
                        {/* {
                            ((window.matchMedia('(display-mode: standalone)').matches) || (window.navigator.standalone) || document.referrer.includes('android-app://')) ?
                                <div className={`Cache`}>
                                    <button type={`button`} className={`icon-menu`} onClick={(e) => this.read(e)}>
                                        <Language translation={this.state.translation} text='download' />
                                    </button>
                                </div> : ''
                        } */}
                        <div className={`ClickZoom`}/>
                    </div>

                    <MetaData
                        pageShowed={this.props.pageShowed}
                        titleName={this.props.name}
                        pageCount={this.props.pageCount}
                        actualPage={this.props.actualPage}
                        connectedPage={this.props.connectedPage}
                    />

                    <Thumbnails
                        pageCount={this.props.pageCount}
                        titlePage={this.props.titlePage}
                        provider={this.props.provider}
                        name={this.props.name}
                        actualPage={this.props.actualPage}
                        connectedPage={this.props.connectedPage}
                        direction={this.props.direction}
                        thumbnailFormat={this.props.thumbnailFormat}
                        thumbnailSprites={this.props.thumbnailSprites}
                    />
                </div>
            </React.Fragment>
        );
    }

    callbackFromAction = (quality) => {
        this.setState({ qualitySwitcher: quality });
        this.props.callbackFromPage(quality);
    };

    read = () => {
        if ($('.Settings').length === 1) {
            $('.QualitySwitcher button').trigger('click');
        }
        $('.NextPage, .PrevPage').hide();
        this.setState({info: !this.state.info});
    };

    resolveLanguage = (props) => {
        let allowedLanguages = ['cs', 'en', 'ru', 'eu', 'local'];
        const hostname = window.location.hostname.split('.');
        let domain = hostname[hostname.length-1];
        let language;

        if (props.language !== null && allowedLanguages.includes(props.language)) {
            language = props.language;
        } else {
            if (allowedLanguages.includes(domain)) {
                if (domain === 'eu' || domain === 'local') {
                    language = 'cs';
                } else {
                    language = domain;
                }
            } else {
                language = 'cs';
            }
        }

        return language;
    };

    isInIframe = () => {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    };

    fullscreenExpand = (e) => {
        this.setState({
            loadingLower: false,
            loadingHigher: true
        });

        if (document.fullscreenEnabled || document.webkitFullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled) {
            if (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement) {
                $('body').removeClass('fullscreen');
                $('.Fullscreen button').removeClass('icon-size-actual').addClass('icon-size-fullscreen');

                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }

                setTimeout(() => {
                    this.setState({
                        loadingLower: false,
                        loadingHigher: false
                    });
                }, 1000);
            } else {
                $('body').addClass('fullscreen');
                $('.Fullscreen button').removeClass('icon-size-fullscreen').addClass('icon-size-actual');

                if (document.body.requestFullscreen) {
                    document.body.requestFullscreen();
                } else if (document.body.webkitRequestFullscreen) {
                    document.body.webkitRequestFullscreen();
                } else if (document.body.webkitEnterFullscreen) {
                    document.body.webkitEnterFullscreen();
                } else if (document.body.mozRequestFullScreen) {
                    document.body.mozRequestFullScreen();
                } else if (document.body.msRequestFullscreen) {
                    document.body.msRequestFullscreen();
                }

                setTimeout(() => {
                    this.setState({
                        loadingLower: false,
                        loadingHigher: false
                    });
                }, 1000);
            }
        }
    };
}

export default Actions;
