/* eslint-disable */

import React, {Component} from 'react';
import {Link} from 'react-router-dom'

export default class Image extends Component {
    render() {
        const {
            name,
            provider,
            titlePage: title_page,
            src: pageNum,
            id,
            connectedPage,
            actualPage,
            thumbnailSprites,
            pageCount,
            direction
        } = this.props;

        let className = '';

        if (direction === 'ltr') {
            if ((title_page && id % 2 === 0) || (!title_page && id % 2 === 1)) {
                className = 'odd';
            } else if ((title_page && id % 2 === 1) || (!title_page && id % 2 === 0)) {
                className = 'even';
            }
        } else {
            if ((title_page && id % 2 === 0) || (!title_page && id % 2 === 1)) {
                className = 'even';
            } else if ((title_page && id % 2 === 1) || (!title_page && id % 2 === 0)) {
                className = 'odd';
            }
        }

        const extension = 'jpg';
        let style = {};

        if (thumbnailSprites !== undefined) {
            const remaining = pageCount - (thumbnailSprites[extension].sprite_parts * (thumbnailSprites[extension].sprite_count - 1));
            const spriteNumber = Math.ceil((pageNum) / 32);
            const sprite = `${window.location.protocol}//${window.location.hostname}:${window.location.port}/data/${provider}/${name}/thumb/${spriteNumber}.sprite.${extension}`;
            let position = '';
            let size = thumbnailSprites[extension].sprite_parts;

            if (spriteNumber === thumbnailSprites[extension].sprite_count) {
                position = (100 / (remaining - 1)) * ((pageNum - ((spriteNumber - 1) * thumbnailSprites[extension].sprite_parts)) - 1);
                size = remaining;
            } else {
                position = (100 / (thumbnailSprites[extension].sprite_parts - 1)) * ((pageNum - ((spriteNumber - 1) * thumbnailSprites[extension].sprite_parts)) - 1);
                size = thumbnailSprites[extension].sprite_parts;
            }
            style = {
                backgroundImage: `url(${sprite})`,
                backgroundPosition: 'calc(' + position + '%',
                backgroundSize: size * 100 + '%',
                width: '100%',
                height: '100%'
            };
        }

        return (
            <div data-page={pageNum} className={pageNum === parseInt(actualPage) || pageNum === connectedPage ? `Image active ${className}` : `Image ${className}`}>
                <Link to={`/${provider}/${name}/${pageNum}${window.location.search}`} data-page={pageNum} style={style}>
                    <img src={`${window.location.protocol}//${window.location.hostname}:${window.location.port}/data/${provider}/${name}/thumb/placeholder.${extension}`} style={{opacity:0}} />
                    <div className={`ImagePage`}>{pageNum}</div>
                </Link>
            </div>
        )
    }
}
