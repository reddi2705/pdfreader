/* eslint-disable */

import React, { Component } from 'react'
import axios from "axios"
import CustomCss from "./CustomCss"
import { Link } from "react-router-dom"
import Loader from "./Loader"
import Actions from "./Actions"
import _ from 'lodash'
import PinchZoomPan from "@grandit/react-responsive-pinch-zoom-pan"
import ReactResizeDetector from 'react-resize-detector'
import InnerPage from "./InnerPage"
import Swipe from '@grandit/react-easy-swipe'
import $ from "jquery"
import Scripts from "../scripts/Scripts"
import { isMobile } from 'mobile-device-detect';


class Page extends Component {
    _isMounted = false;

    constructor(props) {
        // console.clear();
        super(props);

        this.state = {
            config: props.config,
            pages: [],
            loadedPages: [],
            hiddenPages: [],
            hiddenImages: [],
            imageLoadedOdd: false,
            imageLoadedEven: false,
            pagesAdjusted: false,
            pagesAdjustedFirst: false,
            initialLoad: true,
            errors: null,
            portrait: null,
            forceUpdateOneMore: false,
            quality: isMobile ? 'jpg' : 'svg',
            dimensions: {
                page: {
                    width: 0,
                    height: 0
                }
            }
        };

        this.refPage = React.createRef();
        this.refPages = React.createRef();
        this.refInnerPage = React.createRef();
        this.refPinchZoomPan = React.createRef();

        this.bindImageLoaderOdd = this.bindImageLoaderOdd.bind(this);
        this.bindImageLoaderEven = this.bindImageLoaderEven.bind(this);

        this.scripts = new Scripts();
    }

    componentDidMount() {
        this.getPages(this.state.quality);

        this.setState({
            loadedPages: [
                parseInt(this.props.match.params.page),
                this.getConnectedPage(this.props.match, this.state.config)
            ],
        });

        this.setState(
            this.getInitialState(this.refPage.current)
        );
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        const connectedPage = this.getConnectedPage(this.props.match, this.state.config);
        const dimensionsChanged = (
            (
                this.state.dimensions.page.width !== 0
                || this.state.dimensions.page.height !== 0
            )
            && (
                this.state.dimensions.page.width !== nextState.dimensions.page.width
                || this.state.dimensions.page.height !== nextState.dimensions.page.height
            )
        );

        const loaderVisibleNow = !this.isImagesLoaded(this.state) || !this.state.pagesAdjustedFirst;
        const loaderVisibleFuture = !this.isImagesLoaded(nextState) || !nextState.pagesAdjustedFirst;

        const loaderChanged = loaderVisibleNow !== loaderVisibleFuture;

        if (
            (
                (this.isImagesLoaded(nextState) && parseInt(nextProps.match.params.page) === 1)
                || this.isImagesLoaded(nextState)
            )
            && !nextState.pagesAdjusted
        ) {
            this.adjustPages(nextState.dimensions.page.width, nextState.dimensions.page.height);
        }

        const portrait = this.state.portrait !== nextState.portrait;

        const differentPage = this.state.pages !== nextState.pages
            || this.props.match.params.page !== nextProps.match.params.page;

        const quality = this.state.quality === nextState.quality;

        return (
            differentPage
            || portrait
            || dimensionsChanged
            || loaderChanged
            || connectedPage === parseInt(nextProps.match.params.page)
            || quality
            || nextState.pages !== this.state.pages
        );
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const actualPage = parseInt(this.props.match.params.page);
        const prevActualPage = parseInt(prevProps.match.params.page);
        const prevConnectedPage = this.getConnectedPage(prevProps.match, prevState.config);

        if (actualPage === prevConnectedPage) {
            // zobraz connected page, ale nefetchuj
        } else if (actualPage !== prevActualPage) {
            this.setState(
                this.getInitialState(this.refPage.current)
            );

            this.getPages(this.state.quality);

            this.setState({
                imageLoadedOdd: false,
                imageLoadedEven: false,
                pagesAdjusted: false,
                pagesAdjustedFirst: false,
                loadedPages: [
                    parseInt(this.props.match.params.page),
                    this.getConnectedPage(this.props.match, this.state.config)
                ],
            });

            this.refPinchZoomPan.current.setState({scale: 1, left: 0, top: 0})
        } else if (prevState.quality !== this.state.quality) {
            this.getPages(this.state.quality);

            this.setState({
                pagesAdjusted: false,
                pagesAdjustedFirst: false,
                loadedPages: [
                    parseInt(this.props.match.params.page),
                    this.getConnectedPage(this.props.match, this.state.config)
                ],
            });

            this.adjustPages(this.state.dimensions.page.width, this.state.dimensions.page.height);

            this.refPinchZoomPan.current.setState({scale: 1, left: 0, top: 0});
        } else if (prevState.quality === this.state.quality && this.state.forceUpdateOneMore === false) {
            if (
                Scripts.compareObjects(prevState, this.state, [])
                && Scripts.compareObjects(prevProps, this.props, [])
            ) {
                this.setState({
                    pagesAdjusted: false,
                    pagesAdjustedFirst: false,
                    forceUpdateOneMore: true,
                });

                this.adjustPages(this.state.dimensions.page.width, this.state.dimensions.page.height);
            }
        }

        const loaderVisibleNow = !this.isImagesLoaded(this.state) || !this.state.pagesAdjustedFirst;

        if (this.state.initialLoad && !loaderVisibleNow) {
            this.setState({initialLoad: false})
        }
    }

    render() {
        const {config, pages, quality} = this.state;
        const {match} = this.props;
        const {prevPage, nextPage} = this.getAdjacentPages(config);

        const connectedPage = this.getConnectedPage(match, config);
        const showLoader = !this.isImagesLoaded(this.state) || (!this.state.pagesAdjustedFirst && !this.state.pagesAdjusted);
        const index = this.state.initialLoad ? 'Higher' : 'Lower';

        let pageToShow = Object.keys(pages);
        if (config.direction !== undefined && config.direction === 'rtl') {
            pageToShow = pageToShow.reverse();
        }

        let linksActualPage = [];
        let linksConnectedPage = [];
        let actPage = Math.min(parseInt(match.params.page), this.getConnectedPage(match, config));
        let conPage = Math.max(parseInt(match.params.page), this.getConnectedPage(match, config));
        if (this.state.portrait) {
            actPage = parseInt(match.params.page);
        }

        if (config.links !== undefined) {
            if (config.links[actPage] !== undefined) {
                for (let i = 0; i < config.links[actPage].length; ++i) {
                    linksActualPage.push(`<a target="_blank" title="${config.links[actPage][i].name}" href="${config.links[actPage][i].target}">${config.links[actPage][i].name}</a>`);
                }
            }
            if (config.links[conPage] !== undefined) {
                for (let i = 0; i < config.links[conPage].length; ++i) {
                    linksConnectedPage.push(`<a target="_blank" title="${config.links[conPage][i].name}" href="${config.links[conPage][i].target}">${config.links[conPage][i].name}</a>`);
                }
            }
        }

        const {
            actualPagesUrl,
            preloadedPagesUrl
        } = this.buildUrl(this.props.match, config, quality);

        return (
            <React.Fragment>
                <div class="Disclamer">Property of Grandit s.r.o. &copy; 2018</div>
                {showLoader && <Loader index={index} skin={config.skin} />}
                <div className={`PreloadedImage`}>
                </div>
                {
                    config.background_format !== undefined ? (
                        <React.Fragment>
                            <CustomCss
                                path={`${window.location.protocol}//${window.location.hostname}:${window.location.port}/data/${match.params.provider}/${match.params.name}/${quality}/base.min.css`}/>
                            <CustomCss
                                path={`${window.location.protocol}//${window.location.hostname}:${window.location.port}/data/${match.params.provider}/${match.params.name}/${quality}/fancy.min.css`}/>
                            <CustomCss
                                path={`${window.location.protocol}//${window.location.hostname}:${window.location.port}/data/${match.params.provider}/${match.params.name}/${quality}/fonts.css`}/>
                            <CustomCss
                                path={`${window.location.protocol}//${window.location.hostname}:${window.location.port}/data/${match.params.provider}/${match.params.name}/${quality}/${match.params.name}.css`}/>
                        </React.Fragment>
                    ) : ''
                }
                <ReactResizeDetector handleWidth handleHeight onResize={this.onResize}>
                    {({width, height}) =>
                        <Swipe onSwipeLeft={this.onSwipeLeft} onSwipeRight={this.onSwipeRight} useAngle={true}>
                            <div className={`Page`} data-direction={config.direction} ref={this.refPage} style={{width: width, height: height}}>
                                {
                                    (width > height) ?
                                    <div className={`links`}>
                                        <div className={`linksActualPage`} dangerouslySetInnerHTML={{__html: linksActualPage.join('<br/>')}} />
                                        <div className={`linksConnectedPage`} dangerouslySetInnerHTML={{__html: linksConnectedPage.join('<br/>')}} />
                                    </div> :
                                    <div className={`links`}>
                                        <div className={`linksActualPage`} dangerouslySetInnerHTML={{__html: linksActualPage.join('<br/>')}} />
                                    </div>
                                }   
                                {
                                    prevPage ?
                                        <Link className={`PrevPage`} id={config.direction}
                                              to={`/${match.params.provider}/${match.params.name}/${prevPage}${window.location.search}`}>
                                            <button type="button" onClick={this.hideQualitySwitcher} data-role="none"
                                                    className={`owlcarousel-arrow owlcarousel-prev`}> Prev
                                            </button>
                                        </Link>
                                        : ''
                                }
                                <div className={`Pages`} ref={this.refPages}>
                                    <PinchZoomPan ref={this.refPinchZoomPan} debug={false} doubleTapBehavior={`zoom`}
                                                  minScale={1} maxScale={4}>
                                        <div className={`PageResize`}>
                                            {
                                                pageToShow.map((pageIdx, index) => {
                                                    return <InnerPage key={pageIdx}
                                                                      pageHtml={pages[pageIdx]}
                                                                      pageNumber={parseInt(pageIdx)}
                                                                      pageActual={match.params.page}
                                                                      isLandscape={!this.state.portrait}
                                                                      provider={match.params.provider}
                                                                      name={match.params.name}
                                                                      extension={quality !== null ? quality : (isMobile ? 'jpg' : 'svg')}
                                                                      refInnerPage={this.refInnerPage}
                                                                      bindImageLoader={parseInt(pageIdx) % 2 === 0 ? this.bindImageLoaderOdd : this.bindImageLoaderEven}
                                                                      adjustPage={this.adjustInner}
                                                    />
                                                })
                                            }
                                        </div>
                                    </PinchZoomPan>
                                </div>
                                {
                                    nextPage ?
                                        <Link className={`NextPage`} id={config.direction}
                                              to={`/${match.params.provider}/${match.params.name}/${nextPage}${window.location.search}`}>
                                            <button type="button" onClick={this.hideQualitySwitcher} data-role="none"
                                                    className={`owlcarousel-arrow owlcarousel-next`}> Next
                                            </button>
                                        </Link>
                                        : ''
                                }
                            </div>
                        </Swipe>
                    }
                </ReactResizeDetector>

                <Actions
                    pageShowed={_.size(pages)}
                    actualPage={parseInt(match.params.page)}
                    connectedPage={connectedPage}
                    pageCount={config.page_count}
                    titlePage={config.title_page}
                    titleName={config.title_name}
                    skin={config.skin}
                    datePublish={config.date_publish}
                    publisher={config.publisher}
                    provider={match.params.provider || null}
                    language={config.language || null}
                    name={match.params.name}
                    direction={config.direction}
                    backgroundFormat={quality !== null ? quality : (isMobile ? 'jpg' : 'svg')}
                    thumbnailFormat={config.thumbnail_format}
                    thumbnailSprites={config.thumbnail_sprites}
                    callbackFromPage={this.callbackFromPage}
                    allowedBackgroundFormats={config.allowed_background_formats}
                />
            </React.Fragment>
        );
    }

    hideQualitySwitcher = () => {
        if ($('.Settings').length === 1) {
            $('.QualitySwitcher button').trigger('click');
        }
    }

    callbackFromPage = (quality) => {
        this.getPages(quality);

        this.setState({
            hiddenPages: {},
            hiddenImages: {},
            quality,
            loadedPages: [
                parseInt(this.props.match.params.page),
                this.getConnectedPage(this.props.match, this.state.config)
            ],
        });
    }

    bindImageLoaderOdd = () => {
        this.setState({imageLoadedOdd: true})
    };

    bindImageLoaderEven = () => {
        this.setState({imageLoadedEven: true})
    };

    onSwipeLeft = () => {
        if (this.refPinchZoomPan.current.state.scale === 1) {
            if ($('.Page').data('direction') === 'rtl') {
                $('button.owlcarousel-prev').click();
            } else {
                $('button.owlcarousel-next').click();
            }
        }
    };

    onSwipeRight = () => {
        if (this.refPinchZoomPan.current.state.scale === 1) {
            if ($('.Page').data('direction') === 'rtl') {
                $('button.owlcarousel-next').click();
            } else {
                $('button.owlcarousel-prev').click();
            }
        }
    };

    isImagesLoaded = (checkState) => {
        if (this.refPages.current === null) {
            return false;
        } else {
            if (this.refPages.current.children[0].children[0].childElementCount === 1) {
                return checkState.imageLoadedEven || checkState.imageLoadedOdd;
            } else {
                return checkState.imageLoadedEven && checkState.imageLoadedOdd;
            }
        }
    };

    getConnectedPage = (match, config) => {
        const pageNumHex = parseInt(match.params.page.toString(16));
        let connectedPage = 0;
        if (config.title_page && typeof config.title_page === 'undefined') {
            connectedPage = (pageNumHex % 2 === 0) ? pageNumHex - 1 : pageNumHex + 1;
        } else {
            connectedPage = (pageNumHex % 2 === 0) ? pageNumHex + 1 : pageNumHex - 1;
        }

        return connectedPage;
    };

    getAdjacentPages = (config) => {
        const {match} = this.props;

        let prevPage = null;
        let nextPage = null;
        const parsedPage = parseInt(match.params.page);

        if ((config.page_count !== 1 && !config.title_page) || (config.page_count !== 2 && config.title_page)) {
            if (this.state.portrait) {
                if (parsedPage === 1) {
                    prevPage = false;
                    nextPage = parsedPage + 1;
                } else if (parsedPage === 2 && config.title_page) {
                    prevPage = parsedPage - 1;
                    nextPage = parsedPage + 1;
                } else if (parsedPage + 1 > config.page_count) {
                    prevPage = parsedPage - 1;
                    nextPage = false;
                } else {
                    prevPage = parsedPage - 1;
                    nextPage = parsedPage + 1;
                }
            } else {
                if (parsedPage === 1) {
                    prevPage = false;
                    nextPage = parsedPage + 1;
                } else if (parsedPage === 2 && config.title_page) {
                    prevPage = parsedPage - 1;
                    nextPage = parsedPage + 2;
                } else if (parsedPage % 2 === 0 && parsedPage + 2 > config.page_count) {
                    prevPage = parsedPage - 2;
                    nextPage = false;
                } else if (parsedPage % 2 === 1 && parsedPage + 2 > config.page_count) {
                    prevPage = parsedPage - 3;
                    nextPage = parsedPage + 1;
                } else {
                    prevPage = parsedPage - 2;
                    nextPage = parsedPage + 2;
                }
            }
        } else {
            prevPage = false;
            nextPage = false;
        }

        return {
            prevPage,
            nextPage
        }
    };

    onResize = (width, height) => {
        const futurePortrait = (width <= height);
        const isOrientationChange = (futurePortrait !== this.state.portrait);

        this.setState({
            dimensions: {
                page: {
                    width: width,
                    height: height
                },
            },
            portrait: futurePortrait,
            pagesAdjusted: false,
            pagesAdjustedFirst: isOrientationChange ? false : this.state.pagesAdjustedFirst
        });
    };

    getInitialState = (elem) => {
        return {
            portrait: (elem.clientWidth < elem.clientHeight)
        }
    };

    adjustInner = () => {
        this.adjustPages(this.state.dimensions.page.width, this.state.dimensions.page.height);
    }

    adjustPages = (width, height) => {
        const {match} = this.props;
        const {config} = this.state;

        const firstPage = parseInt(match.params.page) === 1 && config.title_page;
        const lastPage = (
                             (config.page_count % 2 === 0 && config.title_page)
                             || (config.page_count % 2 === 1 && !config.title_page)
                         )
                         && config.page_count === parseInt(match.params.page);

        let pageItemWidth = 368
        let pageItemHeight = 532
        const firstElement = this.refInnerPage.current.firstChild;
        if ('dataset' in firstElement) {
            pageItemWidth = Math.floor(firstElement.dataset.width),
            pageItemHeight = Math.floor(firstElement.dataset.height)
        } else {
            pageItemWidth = Math.floor(firstElement.nextElementSibling.width.baseVal.valueInSpecifiedUnits)
            pageItemHeight = Math.floor(firstElement.nextElementSibling.height.baseVal.valueInSpecifiedUnits)
        }

        let dimensions = {
            viewPort: {
                width: width,
                height: height
            },
            pageItem: {
                width: pageItemWidth,
                height: pageItemHeight
            },
            counted: {
                width: 0,
                height: 0,
                ratio: {
                    width: 1,
                    height: 1,
                    use: 0
                }
            },
            count: firstPage || lastPage ? 1 : 2
        };

        dimensions['counted']['width'] = dimensions['pageItem']['width'] * (dimensions['count'] === 2 && width >= height ? 2 : 1);
        dimensions['counted']['height'] = dimensions['pageItem']['height'];

        dimensions['counted']['ratio']['width'] = dimensions['viewPort']['width'] / dimensions['counted']['width'];
        dimensions['counted']['ratio']['height'] = dimensions['viewPort']['height'] / dimensions['counted']['height'];

        dimensions['counted']['ratio']['use'] = Math.min(
            dimensions['counted']['ratio']['width'],
            dimensions['counted']['ratio']['height']
        );

        dimensions['counted']['width'] = dimensions['counted']['ratio']['use'] * dimensions['counted']['width'] / (dimensions['count'] === 2 && width >= height ? 2 : 1);
        dimensions['counted']['height'] = dimensions['counted']['ratio']['use'] * dimensions['counted']['height'];

        let maxPageItemHeight = [];
        let pageItemHeights = [];
        for (let i = 0; i < dimensions['count']; ++i) {
            const pfH = this.refPages.current.children[0].children[0].children[i].children[0];
            const pcH = pfH.children[0];
            pageItemHeights.push(pcH.offsetHeight * dimensions['counted']['ratio']['use'])
        }

        maxPageItemHeight = Math.max(...pageItemHeights);

        for (let i = 0; i < dimensions['count']; ++i) {
            const pf = this.refPages.current.children[0].children[0].children[i].children[0];
            pf.style.width = `${dimensions['counted']['width']}px`;
            pf.style.height = `${dimensions['counted']['height']}px`;

            const pc = pf.children[0];

            dimensions['counted']['minHeight'] = Math.min(maxPageItemHeight, pc.offsetHeight * dimensions['counted']['ratio']['use']);

            pc.style.transform = `scale(${dimensions['counted']['ratio']['use']})`;
        }

        if (dimensions['counted']['ratio']['width'] >= dimensions['counted']['ratio']['height']) {
            const margin = Math.floor(
                (dimensions['viewPort']['width'] - (dimensions['counted']['width'] * (dimensions['count'] === 2 && width >= height ? 2 : 1))) / 2
            );

            this.refPages.current.style.marginLeft = `${margin}px`;
            this.refPages.current.style.marginRight = `${margin}px`;
            this.refPages.current.style.marginTop = `0px`;
            this.refPages.current.style.marginBottom = `0px`;
        } else {
            const margin = Math.round(
                (dimensions['viewPort']['height'] - dimensions['counted']['height']) / 2
            );

            this.refPages.current.style.marginLeft = `0px`;
            this.refPages.current.style.marginRight = `0px`;
            this.refPages.current.style.marginTop = `${margin}px`;
            this.refPages.current.style.marginBottom = `${margin}px`;
        }

        this.setState({pagesAdjusted: true, pagesAdjustedFirst: true});
    };

    getPages = (quality) => {
        this._isMounted = true;
        const {
            match: {
                params: {
                    name,
                    page: pageNum
                }
            }
        } = this.props;
        const { config } = this.state;

        $('body').data('direction', config.direction);
        const page = parseInt(pageNum);
        const connectedPage = this.getConnectedPage(this.props.match, config);

        const {
            actualPagesUrl,
            preloadedPagesUrl
        } = this.buildUrl(this.props.match, config, quality);

        this.setState({
            imagesLoaded: []
        });

        /* * * * * * * * * * * * * * *
         *       PRELOAD PAGES       *
         * * * * * * * * * * * * * * */
        let hiddenPages = {};
        axios
            .all(preloadedPagesUrl.map((preloadedUrl) => {
                return axios.get(preloadedUrl).then((hiddenPageData) => {
                    const hiddenUrlSplit = preloadedUrl.split('/');
                    const hiddenPageId = hiddenUrlSplit[hiddenUrlSplit.length - 1]
                        .replace('.page', '')
                        .replace(name, '');

                    hiddenPages[hiddenPageId] = hiddenPageData.data;
                }).catch(error => {
                    if (error.response && error.response.status === 403) {
                        window.location.reload();
                    }
                });
            }))
            .then(_ => {
                (s => {
                    let t = {};
                    Object.keys(s).sort().forEach(k => { //reverse
                        t[k] = s[k];
                    });
                    return t
                })(hiddenPages);

                delete hiddenPages[0];

                this.setState({
                    hiddenPages: Object.assign(hiddenPages, this.state.hiddenPages),
                    forceUpdateOneMore: false
                });
            })
            .catch(error => {
                console.log(error);
            });

        /* * * * * * * * * * * * *
         *       LOAD PAGES      *
         * * * * * * * * * * * * */
        let pages = {};
        if (!(page in this.state.hiddenPages) && !(connectedPage in this.state.hiddenPages)) {
            axios
                .all(actualPagesUrl.map((url) => {
                    return axios.get(url).then((pageData) => {
                        const urlSplit = url.split('/');
                        const pageId = urlSplit[urlSplit.length - 1]
                            .replace('.page', '')
                            .replace(name, '');

                        pages[pageId] = pageData.data;
                    }).catch(error => {
                        if (error.response && error.response.status === 403) {
                            window.location.reload();
                        }
                    });
                }))
                .then(_ => {
                    (s => {
                        let t = {};
                        Object.keys(s).sort().forEach(k => { //reverse
                            t[k] = s[k];
                        });
                        return t
                    })(pages);

                    delete pages[0];

                    if (this._isMounted) {
                        this.setState({
                            pages,
                            forceUpdateOneMore: false
                        });
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        } else {
            let preloadedPages = {};
            preloadedPages[page] = this.state.hiddenPages[page];

            if (connectedPage !== 0 && connectedPage <= config.page_count) {
                preloadedPages[connectedPage] = this.state.hiddenPages[connectedPage];
            }

            this.setState({
                hiddenPages: this.state.pages,
                pages: preloadedPages
            })
        }
    };

    buildUrl = (match, config, quality) => {
        const page = parseInt(match.params.page);
        const connectedPage = this.getConnectedPage(match, config);
        const loadedPages = Object.keys(this.state.pages).map(p => parseInt(p));
        const minPage = Math.min(page, connectedPage);
        const maxPage = Math.max(page, connectedPage);
        let preloadedPages = [];

        if (maxPage - 2 < 0 && !(loadedPages.includes(maxPage+1) || loadedPages.includes(maxPage+2))) {
            preloadedPages.push(maxPage+1);
            preloadedPages.push(maxPage+2);
        } else if (minPage + 2 > config.page_count && !(loadedPages.includes(minPage-2) || loadedPages.includes(minPage-1))) {
            preloadedPages.push(minPage-2);
            preloadedPages.push(minPage-1);
        } else if (maxPage - 2 > 0 && minPage + 2 <= config.page_count) {
            if (!(loadedPages.includes(minPage-2) || loadedPages.includes(minPage-1))) {
                if (minPage-2 > 0) {
                    preloadedPages.push(minPage-2);
                }
                preloadedPages.push(minPage-1);
            }
            if (!(loadedPages.includes(maxPage+1) || loadedPages.includes(maxPage+2))) {
                preloadedPages.push(maxPage+1);

                if (!config.title_page && maxPage+2 < config.page_count){
                    preloadedPages.push(maxPage+2);
                } else if (config.title_page && maxPage+2 <= config.page_count) {
                    preloadedPages.push(maxPage+2);
                }
            }
        } else {
            preloadedPages.push(minPage-2);
            preloadedPages.push(minPage-1);
        }

        let actualPagesUrl = [
            this.url(quality, `${match.params.name}${page}.page`),
        ];
        let actualImagesUrl = [
            this.url(quality, `bg${(page).toString(16)}.${quality}`),
        ];
        if (connectedPage !== 0 && connectedPage <= config.page_count) {
            actualPagesUrl.push(this.url(quality, `${match.params.name}${connectedPage}.page`));
            actualImagesUrl.push(this.url(quality, `bg${(connectedPage).toString(16)}.${quality}`));
        }
        let preloadedPagesUrl = preloadedPages.map((id) => {
            return this.url(quality, `${match.params.name}${id}.page`);
        });
        let preloadedImagesUrl = preloadedPages.map((id) => {
            return this.url(quality, `bg${(id).toString(16)}.${quality}`);
        });

        return {
            actualPagesUrl,
            preloadedPagesUrl
        }
    }

    url = (quality, file) => {
        return `${window.location.protocol}//${window.location.hostname}:${window.location.port}/data/${this.props.match.params.provider}/${this.props.match.params.name}/${quality}/${file}`;
    }
}

export default Page;
