import React from 'react'
import { render } from 'react-dom'
import './reset.css'
import './index.css'
import './responsive.css'
import './carousel.css'
import './skins/crew.css'
import './skins/ck-fischer.css'
import './skins/cnc-specialy.css'
import App from './App'
import * as serviceWorker from './serviceWorker';


const config = JSON.parse(document.getElementById('root').dataset.config);

render(
    <App config={config} />,
    document.getElementById('root')
);

serviceWorker.register();
