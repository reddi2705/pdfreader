/* eslint-disable */

import React, {Component} from 'react';
import {Router, Switch, Route} from "react-router-dom";
import Publication from "./components/Publication";
import Page from "./components/Page";
import NotFound from "./components/error/NotFound";
import Forbidden from "./components/error/Forbidden";
import history from './history';

class App extends Component {
    render() {
        const {config} = this.props;

        return (
            <div className={`BounceFixed ${config.skin}`}>
                <div className={`BounceTrick`}>
                    <Router history={history}>
                        <Switch>
                            <Route path="/:provider/:name" exact component={Publication}/>
                            <Route path="/:provider/:name/:page([0-9]{1,3})" exact render={(props) => <Page {...props} config={config} />} />
                            <Route path="/forbidden" component={Forbidden} />
                            <Route component={NotFound} />
                        </Switch>
                    </Router>
                </div>
            </div>
        );
    }
}

export default App;
