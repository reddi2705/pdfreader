Use `npm|yarn install` to install app

Use `npm|yarn start` to run app

Then navigate to `http://localhost:8080/komiks/nesnasim-pohadkov`

 - Originalni aplikace je v branchi `master`
 - Modifikovana aplikace je v branchi `hooks`


## Podnety pro zlepseni aplikace
Vzhledem k tomu, ze to byl muj pilotni projekt v ReactJS, bez predchozich znalosti
v agenturnim prostredi, kde se uprednostnuje rychlost realizace nad kvalitou,
navrh aplikace by vypadal uplne jinak.

1. Komponenta `Page.jsx` by potrebovala dekomponovat na mensi casti, jelikoz
   obsahuje spoustu vypoctu a logiky
2. Pomocne funkce pro vypocty bych separoval do nejake helper knihovny
3. Komponentam bych nepredaval dekomponovane property z props, ale rovnou cele props
   a v child komponente bych az destrukturalizoval data
4. Po dekomponovani by nektere komponenty mohly byt `state-less`
5. Pri verzi 16.7. bych nyni pouzil `redux` pro zjednodusseni handlovani
   globalniho statu
6. Od verze 16.8. bych rozhodne pouzil React hooky, pro zprehledneni flow aplikace,
   jelikoz nyni trosku overriduju metodu `shouldComponentUpdate()`, pro direktivni
   signal Reactu pro rerender komponenty a dle meho to zneprehlednuje originalni
   react-flow a myslim si, ze dochazi k nekolika nechtenym prerendrovanim komponenty
7. Translacni komponentu bych nahradil za 3rd party
8. Vzhledem k obrovskemu mnozstvi tahanych dat pro zobrazeni (ukazka ma cca. 30MB)
   bych urcite pridal nejake `cachovani` (i kdyz bohuzel je zde zoom a jpg|png
   obrazky musi byt v urcite velikosti, kdy jeste neni zhorsena kvalita)
9. Aplikace je PWA, ale urcite by se dalo optimalizovat generovani manifestu a
   nastaveni cachovani souboru, vzhledem k rozdilnosti velikosti localstorage na jednotlivych mobilnich zarizenich by se ale urcite dal najit kompromis co stahovat do zarizeni a co ne, aby se uzivateli nevycucalo FUP pri pristupu z cellularni site
10. A v posledni rade bych kompletne odstranil jQuery, ktera je zde spise jako    berlicka pro ruzne schovavaci a animacni efekty a pokusil bych se je udelat `in react-way`