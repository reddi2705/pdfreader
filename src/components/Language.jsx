import {Component} from 'react';

class Language extends Component {
    render() {
        return this.props.translation[this.props.text] ? this.props.translation[this.props.text] : '';
    }
}

export default Language;
