/* eslint-disable */

import React, {Component} from 'react';
import OwlCarousel from '@grandit/react-owl-carousel2';
/* import '@grandit/react-owl-carousel2/src/owl.carousel.css'; */
import Image from "./Image";
import Scripts from "../scripts/Scripts"

global.jQuery = require('jquery');
var $ = global.jQuery;
window.$ = $;

class Thumbnails extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nav: true,
            lazyLoad: true,
            rtl: this.props.direction === 'rtl',
            loop: false,
            center: true,
            navText: [
                '<button type="button" data-role="none" class="owl-arrow"> Prev</button>',
                '<button type="button" data-role="none" class="owl-arrow"> Next</button>',
            ],
            responsive: this.getResponsive()
        }
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        const areSame = Scripts.compareObjects(this.props, nextProps, ['page', 'actualPage', 'connectedPage']);

        if (areSame && this.props.actualPage !== nextProps.actualPage) {
            $('.carouselGoTo').data('actualpage', nextProps.actualPage);
            $('.carouselGoTo').data('connectedpage', nextProps.connectedPage);
            $('.carouselGoTo').trigger('click');
        }

        return !areSame || nextState.responsive !== this.state.responsive;
        // || this.props.direction === undefined && this.props.direction !== nextProps.direction;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.thumbnailSprites === undefined && this.props.thumbnailSprites !== undefined){
            $('.carouselGoTo').trigger('click');
        }
    }

    render() {
        const {
            provider,
            name,
            titlePage,
            direction,
            pageCount,
            actualPage,
            connectedPage,
            thumbnailFormat,
            thumbnailSprites
        } = this.props;

        const titlePageDef = (typeof titlePage === 'undefined' || titlePage);

        const options = {
			nav: this.state.nav,
            lazyLoad: this.state.lazyLoad,
            rtl: this.props.direction === 'rtl',
            loop: this.state.loop,
            center: this.state.center,
            navText: this.state.navText,
            responsive: this.getResponsive(),
            startPosition: actualPage-1
        };

        const events = {
            onResized: () => {}
        };

        return (
            <div className={`Thumbnails ${direction}`}>
                <OwlCarousel ref="carousel" options={options} events={events}>
                    {
                        [...Array(pageCount).keys()].map((currPublication, i) => {
                            return <Image
                                key={currPublication}
                                src={currPublication + 1}
                                provider={provider}
                                name={name}
                                id={`${currPublication + 1}`}
                                titlePage={titlePageDef}
                                actualPage={actualPage}
                                connectedPage={connectedPage}
                                thumbnailFormat={thumbnailFormat}
                                thumbnailSprites={thumbnailSprites}
                                carousel={this.refs.carousel}
                                pageCount={pageCount}
                                direction={direction}
                            />
                        })
                    }
                </OwlCarousel>
                <button onClick={() => this.handleCarouselClick()} data-actualpage={actualPage} data-connectedpage={connectedPage} className={`carouselGoTo`}>g</button>
                <button onClick={this.setOptions.bind(this)} className={`carouselNewOptions`}>o</button>
                <a href="https://www.digitania.cz/" target="_blank" rel="noopener noreferrer" className={`Company`}>Digitania reader</a>
            </div>
        );
    }

    handleCarouselClick = () => {
        $('.Image').removeClass('active');
        $('.Image[data-page=' + $('.carouselGoTo').data('actualpage') + ']').addClass('active');
        $('.Image[data-page=' + $('.carouselGoTo').data('connectedpage') + ']').addClass('active');
        this.refs.carousel.goTo($('.carouselGoTo').data('actualpage') - 1)
    };

    setOptions = () => {
        this.setState({responsive: this.getResponsive()});
    };

    getResponsive = () => {
        let res800, res575, res480, res320 = 0;
        let viewport_height = $(window).height();
        $(window).resize(function() {
            viewport_height = $(window).height();
        });

        if (Scripts.isPortrait()) {
            /* screen portrait */
            res800 = 5;
            res575 = 4.3;
            res480 = 3.3;
            res320 = 2.3;
        } else {
            /* screen landscape */
            if (viewport_height <= 300) {
                /* viewport height <= 300 */
                res800 = 2.3;
                res575 = 2.3;
                res480 = 2.3;
                res320 = 2.3;

            } else {
                /* viewport height > 300 */
                res800 = 7;
                res575 = 6;
                res480 = 6;
                res320 = 6;
            }
        }

        return {
            1760: {
                items: 12,
                slideBy: 12
            },
            1600: {
                items: 10,
                slideBy: 10
            },
            1440: {
                items: 9,
                slideBy: 9
            },
            1280: {
                items: 8,
                slideBy: 8
            },
            1120: {
                items: 7,
                slideBy: 7
            },
            960: {
                items: 6,
                slideBy: 6
            },
            800: {
                items: res800,
                slideBy: res800
            },
            575: {
                items: res575,
                slideBy: res575
            },
            480: {
                items: res480,
                slideBy: res480
            },
            320: {
                items: res320,
                slideBy: res320
            }
        };
    };
}


export default Thumbnails;
